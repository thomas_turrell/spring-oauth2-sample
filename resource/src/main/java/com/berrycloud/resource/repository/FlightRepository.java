package com.berrycloud.resource.repository;
 
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.berrycloud.resource.entity.Flight;

@RepositoryRestResource
public interface FlightRepository extends CrudRepository<Flight, Long> {

	@Override
	Iterable<Flight> findAll();

	@Override
	Flight findOne(Long aLong);

	@Override
	<S extends Flight> S save(S entity);

}
